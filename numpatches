#!/usr/bin/bash
# SPDX-License-Identifier: GPL-2.0

# This script returns the number of patches between the current branch and main
#
# There are some other alternatives that may end up being useful.  For example
#
#    git rev-list --count $(git describe --abbrev=0 --tags)..
#
# returns the number of patches between the current branch and the last tag
# in the tree

# This returns the number of patches between the current branch and main
# 	git rev-list --count main..

# A useful alias for this command is
# num-patches = "!f() { head=$(git merge-base HEAD main); git rev-list --count ${head}..; }; f"

if [ $# -ne 1 ]; then
	echo "Specify a target branch."
	exit 1
fi
branch=$1

if git rev-parse --verify --quiet $branch >& /dev/null; then
	git rev-list --count "$branch"..
else
	echo "Branch $branch not found"
	exit 1
fi
